# 基础学习 快速开始
# 变量
print("----- 变量 -----")
num_1, num_2 = 10, 20
sum_result = num_1 + num_2
print(sum_result)

# 关键词
print("----- 关键词 -----")
import keyword

keyword_list = keyword.kwlist
print(keyword_list)

# 基础数据类型
print("----- 基础数据类型 -----")
a_num = 666
b_str = '777'
c_list = [a_num, b_str, True, None, 5.5, 1 + 98j, ['x', 'y', ['z']], ('m', 'n')]

print('z ==>', c_list[6][2][0])
print('n ==>', c_list[-1][1])

print(c_list)
c_list.append(3)
print(c_list)
c_list.remove(3)
print(c_list)

for idx, val in enumerate(c_list):
    print('第{}个元素的类型为：{}，其值为：{}'.format(idx, type(val), val))

# 控制语句
print("----- 分支语句 -----")
score = 60
if score >= 60:
    print('及格了')
else:
    print('差点及格')

print("----- 循环语句 while -----")
# Fibonacci series
a, b, i = 0, 1, 1
while True:
    if i < 10:
        print(b, end=',')
        a, b, i = b, a + b, i + 1
    else:
        print(b)
        break

print("----- 循环语句 for -----")
total = list(range(1, 11))
for i in total:
    print(i, end=' ')

# 自定义函数
print("----- 自定义函数 -----")


def get_max(num1, num2):
    if num1 > num2:
        return num2
    else:
        return num2


print(get_max(6, 9))

# 引入模块
print("----- 引入模块 -----")
import sys

print(sys.path)
