###############################
# 将生成的 WORD文件 转为 PDF文件 #
###############################


# 获取目录中的 docx文件
import os

from docx2pdf import convert


def get_docx_list(in_filepath):
    docx_list = []
    for root, dirs, files in os.walk(in_filepath):
        for file in files:
            filepath = os.path.join(root, file)
            # print(filepath)
            if filepath.find('.docx') > 0:
                docx_list.append(filepath)
    return docx_list


if __name__ == '__main__':
    in_filepath = './result/'
    out_filepath = './result'

    docx_list = get_docx_list(in_filepath)
    for i in range(len(docx_list)):
        convert(in_filepath)
        # print(docx_list[i])

    print('生成完毕')
