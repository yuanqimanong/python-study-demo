#####################################
# 将 PDF目录 压缩并命名为 “年月日.zip”  #
#####################################
import datetime
import os

# 获取文件
from zipfile import ZipFile


def get_all_files(dir):
    for root, dirs, files in os.walk(dir):
        for file in files:
            yield os.path.join(root, file)


# 生成zip文件名
def get_zip_name():
    today = datetime.date.today()
    basename = today.strftime('%Y%m%d')
    extname = 'zip'
    return f"{basename}.{extname}"


# 无密码压缩
def zip_without_password(backup_files, zip_filename):
    with ZipFile(zip_filename, 'w') as zf:
        for f in backup_files:
            zf.write(f)


if __name__ == '__main__':
    # 要备份的目录
    backup_dir = "./result/pdf"

    # 获取文件
    backup_files = get_all_files(backup_dir)
    # 生成zip文件名
    zip_filename = get_zip_name()

    # 无密码压缩
    zip_without_password(backup_files, "./result/" + zip_filename)

    print('生成完毕')
